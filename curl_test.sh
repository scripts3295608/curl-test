#!bin/bash

echo 'enter domain name to test:'

read domain

DNS=$(dig +short A $domain)

responsetime=$(curl -s -w '\nLookup Time:\t\t%{time_namelookup}\nConnect Time:\t\t%{time_connect}\nAppCon Time:\t\t%{time_appconnect}\nRedirect Time:\t\t%{time_redirect}\nPre-transfer Time:\t%{time_pretransfer}\nStart-transfer Time:\t%{time_starttransfer}\n\nTotal Time:\t\t%{time_total}\n' -o /dev/null $domain)

responsecode=$(curl -ILs $domain | grep -i 'http/\|location')

ssl=$(echo | openssl s_client -servername $domain -connect $domain:443 2>/dev/null | openssl x509 -noout -dates)

echo -e "==============================================\nWEBSITE RESPONSE TESTER\n$domain\n==============================================\nDNS A RECORD\n$DNS\n==============================================\nSSL:\n$ssl\n==============================================\nRESPONSE CODE:\n$responsecode\n==============================================\nRESPONSE TIME:\n$responsetime\n=============================================="

#cloudflare=$(curl -s https://www.cloudflare.com/ips-v4)

#for ip in $cloudflare; do
#if echo "$DNS" | grep -iq $ip; then
#        echo "cloudflare=yes"
#       return 0
#fi
#done
